---
layout: job_family_page
title: "Account Based Marketing Manager"
---

The Account Based Marketing Manager is responsible for the strategy and implementation of effective Account Based Marketing programs that meet the growth objectives of the business, with a strong emphasis on driving pipeline generation exclusively from a top target account list, and accelerating existing opportunities. The role will also support strong partnership with key stakeholders within the Marketing organization.

## Responsibilities
*   Define Account Based Marketing strategy and execution planning, encompassing our multiple ABM tools.
*   Own and manage our Account Based Marketing platform, DemandBase.
*   Partner with the regional Field Marketing Managers to develop regional, account focused playbooks including, but not limited to, outbound prospecting, inbound lead follow up, nurture, channel activities, live/online events, meeting setting with the goal of building pipeline and accelerating bookings.
*   Collaborate and partner with core marketing functions on campaign concept, custom content requirements, digital marketing support/message testing and creative development to ensure effectiveness for regional named accounts.
*   Identify the most effective marketing messages, value proposition, materials, channels, and calls to action for personas and key accounts in the regions.
*   Build then obsess over ABM metrics to ensure business impact measured by named account pipeline/bookings (sourced/influenced), database growth and accuracy for named accounts, account/person engagement scores and sales/partner utilization of programs.
*   Manage project timelines, quality issues, resources, &  budget.

## Requirements
*   5+ years of proven multi-channel B2B marketing experience within enterprise software/SaaS.
*   DemandBase experience is a plus.
*   You are a proactive self-starter, demonstrating high initiative and critical thinking. Proven ability to think strategically, but with exceptional attention to detail in execution.
*   Positive, can-do attitude, with the ability to work with marketing and sales professionals, and ability to prioritize numerous projects simultaneously.
*   Experience building and delivering of field marketing campaigns, events, executive and key account programs and partner activity.
*   Understanding of pipeline management and metrics.
*   Demonstrated experience sourcing and overseeing vendors for marketing program execution.
*   Excellent verbal and written communication skills. Strong interpersonal skills a must. Strong influencing and relationship-building skills.
*   Team player with ability to collaborate, and also work independently. 
*   Flexibility to adjust to the dynamic nature of a startup.
*   Occasional domestic travel to support events - less than 30%.
*   Ability to use GitLab.

## Levels
### Account Based Marketing Manager

#### Job Grade
The Account Based Marketing Manager is a [6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Support abm campaign logistics and campaign tactics
- Help facilitate reporting and metrics on the account based marketing campaigns and tactics
- Assist with account research and campaign planning, development and execution
- Partner closely with SDR and sales teams to ensure strong marketing and sales alignment.
- Document processes and best practices in our handbook for marketing and sales teams to reference
- Work closely with Marketing and Sales Operations to support high-performance ABM marketing technology.
- Align and collaborate with sales and sales enablement teams to ensure SLAs for lead and campaign follow up are achieved


#### Requirements
- 2+ years of experience in account based marketing or similar field, with the desire to learn more about account based marketing
- Highly organized and efficient with the ability to develop and execute against an evolving plan
- Able to clearly communicate new ideas, products, and campaigns to both internal and external stakeholders
- Team player that is able to work both collaboratively and autonomously
- Strong project management, planning, and organizational skills
- Demonstrated ability to achieve results working cross-functionally with sales, marketing, and product teams.
- Demonstrated ability to manage multiple projects simultaneously
- CRM/marketing automation background, preferably Salesforce and Marketo
- Experience working both independently and within close teams in a fast-paced remote work environment, managing multiple projects simultaneously
- Ability to use GitLab.


#### Performance Indicators

- Average time to close for accounts within the ABM strategy in comparison to firmographic lookalike accounts
- Average deal size of closed won deals within the ABM strategy
- Account penetration: number of touchpoints to close
- Increased number of opportunities per company within the ABM strategy
- Percentage of MQL's that flip to SAO's


### Manager, Account Based Marketing

#### Job Grade
The Account Based Marketing Manager is a [8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
- Build out and develop the global account based marketing team.
- Refine and hone in on the account based strategy based on cross functional goals
- Own all aspects of the account based marketing tech stack, including evaluation of additional tools
- Partner with the Field Marketing Managers and regional stakeholders to develop account focused playbooks including, but not limited to, outbound prospecting, inbound lead follow up, nurture, channel activities, live/online events, meeting setting with the goal of building pipeline and accelerating bookings.
- Manage the relationship and collaboration across core marketing functions on campaign concepts, custom content requirements, digital marketing support/message testing and creative development to ensure effectiveness for regional named accounts.
- Identify the most effective marketing messages, value proposition, materials, channels, and calls to action for personas and key accounts in the regions.
- Build out the framework of metrics and reporting for what success looks like for the account based strategy
- Manage and continue to improve the abm team’s efficiency and ROI
- Manage project timelines, quality issues, resources, & budget.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders.
- People management experience


#### Requirements
- 5+ years of proven multi-channel B2B marketing experience within enterprise software/SaaS.
- You are a proactive self-starter, demonstrating high initiative and critical thinking. Proven ability to think strategically, but with exceptional attention to detail in execution.
- Positive, can-do attitude, with the ability to work with marketing and sales professionals, and ability to prioritize numerous projects simultaneously.
- Experience building and managing field marketing campaigns, events, executive and key account programs as well as digital marketing.
- Understanding of pipeline management and metrics.
- Demonstrated experience sourcing and overseeing vendors for marketing program execution.
- Excellent verbal and written communication skills. Strong interpersonal skills a must. Strong influencing and relationship-building skills.
- Team player with ability to work independently and autonomously.
- Flexibility to adjust to the dynamic nature of a startup.
- Occasional domestic travel to support events - less than 30%.
- Ability to use GitLab
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)


#### Performance Indicators

- Average time to close for accounts within the ABM strategy in comparison to firmographic lookalike accounts
- Average deal size of closed won deals within the ABM strategy
- Account penetration: number of touchpoints to close
- Increased number of opportunities per company within the ABM strategy
- Percentage of MQL's that flip to SAO's

## Career Ladder

The next step for both individual contributors and managers of people is to move to the [Director of Field Marketing](/job-families/marketing/director-field-marketing) job family or laterally to the [Field Marketing Manager](/job-families/marketing/field-marketing-manager) job family.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/). Additional details about our process can be found on our [hiring page](/handbook/hiring).

*   Selected candidates will be invited to schedule a screening call with one of our Global Recruiters.
*   Next, candidates will be invited to schedule a first-round interview with the hiring manager - the Manager, Field Marketing North America.
*   Candidates will then be invited to schedule a series of 45 minute interviews with our Manager, Digital Marketing Programs and the Sr. Manager of NORAM Sales Development.
*   Candidates will then be required to submit a 30/60/90 day plan to the recruiter for the hiring manager to review. Upon review, the candidate may be asked additional questions about the plan.
*   Candidates will then be invited to schedule an interview with our Sr. Director of Revenue Marketing.
*   Our CMO may choose to conduct an interview.
*   Finally, our CEO may choose to conduct a final interview.
*   Successful candidates will subsequently be made an offer via email.
