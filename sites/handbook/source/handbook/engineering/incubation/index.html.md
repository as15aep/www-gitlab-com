---
layout: handbook-page-toc
title: Incubation Engineering Department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Incubation Engineering Department

The Incubation Engineering Department within the Engineering Division focuses on projects that are pre-Product/Market fit. The projects they work on align with the term "new markets" from one of our [product investment types](/handbook/product/investment/#investment-types). They are ideas that may contribute to our revenue in 3-5 years time. Their focus should be to move fast, ship, get feedback, and [iterate](/handbook/values/#iteration). But first they've got to get from 0 to 1 and get something shipped.

We utilize [Single-engineer Groups](/company/team/structure/#single-engineer-groups) to draw benefits, but not distractions, from within the larger company and [GitLab project](https://gitlab.com/gitlab-org/gitlab) to maintain that focus. The Single-engineer group encompasses all of product development (product management, engineering, design, and quality) at the smallest scale. They are free to learn from, and collaborate with, those larger departments at GitLab but not at the expense of slowing down unnecessarily.

The Department Head is the [VP of Incubation Engineering](/job-families/engineering/vp-of-incubation-engineering/).

### FY22 Direction

The Incubation Engineering Department is new, and the focus for FY22 is to:

* Hire team members that work as Single-Engineer Groups to deliver the departments priorities.
* Develop a repeatable process and appropriate performance indicators that allow the team to measure and demonstrate their progress and impact for each of the areas we are interested in delivering.
* Develop a scoring framework and methodology to accept and prioritise new ideas.

The initial iteration towards this Direction is covered in the [FY22-Q2 Incubation Engineering Department OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11187).

## Single-Engineer Groups

### [MLOps / GitLab Data](/handbook/engineering/incubation/mlops)

The MLOps Group will be focused on enabling data teams to build, test, and deploy their machine learning models. This will be net new functionality within GitLab and will bridge the gap between DataOps teams and ML/AI within production. MLOps will provide tuning, testing, and deployment of machine learning models including version control and partial rollout and rollback.

### [Monitor APM](/handbook/engineering/incubation/monitor-apm)

The Monitor APM Group aims to integrate monitoring and observability into our DevOps Platform in order to provide a convenient and cost effective solution that allows our customers to monitor the state of their applications, understand how changes they make can impact their applications performance characteristics and give them the tools to resolve issues that arise.

### [5 min prod app](/handbook/engineering/incubation/5-min-production)

The 5 Minute Production App Group will look at our developer onboarding experience and how to make it easier for Web App developers to quickly get their application deployed to Production, and to have the re-assurance that it can scale when needed.

### Real-time Collaboration

The Real-time Collaboration Group is gathering feedback on what merge requests and code review in GitLab may be in the future and how we can significantly decrease the cycle time, increase the efficiency of code review, and create better ways of collaborating through real-time experiences.


## Issue Board

[Incubation Engineering Issues](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/980804?scope=all&utf8=✓&label_name[]=Engineering%20Management&label_name[]=Incubation%20Engineering%20Department)
