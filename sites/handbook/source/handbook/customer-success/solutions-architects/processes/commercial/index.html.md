---
layout: handbook-page-toc
title: Commercial SA Engagement Model
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

## Commercial Solutions Architecture Engagement Model

Wherever possible, let's bring synchronicity back to our sales and customer-facing team members and relationships.

Anytime we use text-based communication, we might miss a chance to connect with someone - which can be critical to our personal and business relationships. While asynchronous communication is important, it can sometimes hamper our ability to clearly communicate. In contrast, a conversation allows us to establish a relationship with the other person and perhaps communicate clearer.

The Commercial SA Engagement Model intends to foster collaboration and influence and even greater iteration amongst ourselves and customers.

#### SA Engagement Considerations

- All Solutions Architect (SA) requests are submitted via [Americas](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/-/issues/new?issuable_template=SA%20Activity) and [EMEA](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/emea-commercial-triage/-/issues/new?issuable_template=SA%20Activity) triage boards
- SA notes and activities are logged within Salesforce via Troops.ai (review the [SA Activity Capture page](https://about.gitlab.com/handbook/customer-success/solutions-architects/processes/activity-capture/)).
- Deal qualification is simply any deal that **is piped at $10k and higher** focused not only on engagement value but also customer potential for consideration of Solutions Architect assignment.
- All Salesforce linked active opportunities should have [MEDDPPICC](https://about.gitlab.com/handbook/sales/meddppicc/) (and [required 7 methods applied](https://about.gitlab.com/handbook/sales/commercial/#required-7))
- Compelling events are clearly defined or the issue (i.e. triage request) has a clear statement indicating why that information isn’t available yet and how the SA can help obtain it.

#### Meeting Expectations

- All meetings should be planned with clear desired outcomes available to the SA
    - Why does the prospect want to meet with us?
    - What are our meeting objectives/goals?
    - Agenda and list of attendees should be provided in advance;<u>failure to provide this information could delay in the scheduling or declination of a meeting request.</u>
- SA activities include:
  - Discovery calls
  - Demonstrations
  - [Proof-of-Value engagements (b.k.a POVs)](https://about.gitlab.com/handbook/sales/POV/#pov-best-practices)
    - POV's should  be positioned for engagements that are $25k or higher
    - Pro tip: All POV/evaluation scope should be negotiated and vetted, so we understand “why”. Let’s not rush to answer.  We should validate our prospects’/customers’ needs and know what they’re asking for. This will help us provide a solution by establishing success criteria to evaluate and confirm GitLab as their best path forward. "Production" and "Pre-production" level POVs/evals should be discouraged or provided as paid engagement option as part of a license purchase.

_Please Note: Exceptions can and should be made via the issue commenting, mentioning both the ASM (aka Area Sales Manager as well as the Commercial SA Leader)._

#### Opportunities below $10K

In the spirit of supporting the sales floor and deals below $10k we’d like to continue to use Slack offering same day SLA’s for initial response and validation of the ask with mutually agreed upon delivery on formal answers to the question(s) posed.

**Now Available:** [SA Office Hours (US)](https://gitlab.com/gitlab-com/customer-success/solutions-architecture-leaders/sa-initiatives/-/issues/33) and SA Slack Channels (EMEA)

To help with deals below $10k, we have introduced office hours as a forum that allows AE's to ask questions of the SA team in a one-to-many fashion that may help accelerate a smaller engagement, increase their knowledge of GitLab and other industry related technologies.

_Please Note: If an exception needs to be made please copy both the ASM and Commercial SA Leader should a response be needed quicker though we can’t guarantee that this will be fulfilled._

#### Post-Sales Engagement

As an opportunity enters into either the [Negotiating or Awaiting Signature](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#opportunity-stages) stage, the Solutions Architect and Account Executive ought to begin introducing the customer to a [Technical Account Manager](https://gitlab.com/gitlab-com/customer-success/tam/-/blob/master/README.md#americas-mid-market) when the customer meets necessary thresholds or the SA and AE can begin preparing the [contacts for receiving digital onboarding emails](https://about.gitlab.com/handbook/customer-success/tam/digital-journey/nominating-contacts-for-the-digital-journey/) upon closing of a deal.

Solutions Architects ought to be primarily engaged in opportunities that have active opportunities in Salesforce.  When we work with customers, it's easy to build a trusted advisor relationship with them that persists past the end of the sale.  In these cases, SAs must use their judgment in determining when to redirect a customer to the proper support channel for follow-up questions.

Below are a few example responses an SA can provide customers that reach out for help after a deal closes. Please leverage your personal connection to them and their company to customize these as you see fit.

##### Accounts without a Technical Account Manager

> Thanks for reaching out!
> 
> In order to best direct your question and provide you a timely response, can you submit a support ticket with our support team? Additionally, I have copied your Account Executive as they can help escalate your request if necessary. Below are some links to get started with GitLab support.
> 
> I thoroughly enjoyed getting a chance to work with you and role is primarily focused on our customers that are involved in pre-sales engagements; and being a person of one, I don't want to be a bottleneck to you getting a response.
> 
> You can go to [support.gitlab.com](https://support.gitlab.com/hc/en-us/requests/new) and submit a new request. Please use your company email address and an account and password will be created for you. There are more details regarding [reaching out to support](https://about.gitlab.com/support/#first-time-reaching-suppor).

##### Accounts with a Technical Account Manager

> Thanks for reaching out!
> 
> In order to best direct your question and provide you a timely response, can you submit a support ticket with our support team? Additionally, I have copied your Technical Account Manager and Account Executive, as well, as they can help escalate your request if necessary. Below are some links to get started with GitLab support.
> 
> I thoroughly enjoyed getting a chance to work with you and role is primarily focused on our customers that are involved in pre-sales engagements; and being a person of one, I don't want to be a bottleneck to you getting a response.
> 
> You can go to [support.gitlab.com](https://support.gitlab.com/hc/en-us/requests/new) and submit a new request. Please use your company email address and an account and password will be created for you. There are more details regarding [reaching out to support](https://about.gitlab.com/support/#first-time-reaching-suppor).

**Below are some additional items you can share with the customer.**

- Search GitLab documentation and issues with these pro [tips](/handbook/tools-and-tips/searching/)!
- If you do not find a proposed feature in the [GitLab issues list](https://gitlab.com/gitlab-org/gitlab/-/issues), please [contribute an idea](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal&issue[title]=Docs%20feedback%20-%20feature%20proposal:%20Write%20your%20title) to our product team to improve the community's experience!
- [GitLab Documentation](https://docs.gitlab.com/) covering How-Tos for Installation and Day-to-Day usage.
- GitLab is fortunate enough to have a strong community of contributors where you can search for ideas and issues within the GitLab [Forum](https://forum.gitlab.com/) or moderated [subreddit](https://www.reddit.com/r/gitlab/).
- With transparency being a value of ours, we strive to push content daily to both the [GitLab Youtube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) and [GitLab Unfiltered Channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A). You will find How-Tos and Daily Engineering conversations in these channels.
- If you need engineering assistance, please [create a support ticket](https://support.gitlab.com/hc/en-us). Your team has ["Standard Support"](https://about.gitlab.com/support/#standard-support) which means "Next Day" or 24-hour support Monday thru Friday.

#### Other Considerations

##### Team meetings

- We hold our team meetings on Mondays (Americas) at 11am EST and Tuesdays (EMEA) 9.30am EST for an hour - we expect that unless we’re being asked to participate in a critical customer call that our folks attend our meetings.
- Try to be respectful of our scheduled 1:1's meetings, though they can be more easily rescheduled in favor of customer engagement.

##### Meeting Follow up/Research

- Solutions Architects need time to provide follow up with information in as near real-time as possible. We aim to minimize scheduling “back-to-back” meetings as this can compromise our ability to provide our best possible response for the customers/prospects we're supporting.
- [Mid-Market Case Studies](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=%E2%9C%93&label_name%5B%5D=Mid-Market) | [SMB Case Studies](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878?scope=all&utf8=%E2%9C%93&label_name%5B%5D=SMB)
- [Whitepapers, Analyst Reports, etc...](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit#gid=0)
