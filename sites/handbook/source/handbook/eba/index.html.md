---
layout: handbook-page-toc
title: "Executive Business Administrators"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

This page details processes and general guidelines specific to the Executive Business Administrators (EBA's) at GitLab. The page is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense.

## Executive Business Administrator Team

* Cheri Holmes (Staff EBA) supporting Sid Sijbrandij (Co-Founder & CEO) and Michael McBride (CRO)
* Stefanie Haynes (Sr. EBA) supporting Todd Barr (CMO) and Darren Murph (Head of Remote)
* Robyn Hartough (Sr. EBA) supporting Robin Schulman (CLO), and Legal Directors: Rashmi Chachra (Corporate), Rob Nalen (Operations & Contracts), Emily Plotkin (Employment), and Lynsey Sayers (Privacy & Product)
* Jaclyn Grant (Sr. EBA) supporting Brian Robins(CFO) and Finance Leadership: Craig Mestel, Dale Brown, Bryan Wise, Tony Righetti, Igor Groenewegen-Mackintosh
* Trevor Knudsen (EBA) supporting Wendy Barnes (CPO), Pattie Egan (VP of People Operations, Technology and Analytics), Rob Allen (VP, Talent Acquisition) and Carol Teskey (Senior Director, People Success)
* Katie Gammon (EBA) supporting Stella Treas (Chief of Staff)
* Kristie Thomas (EBA) supporting Scott Williamson (CProdO),Eric Johnson (CTO), Johnathan Hunt (VP of Security), Anoop Dawar (VP of Product Management), David DeSanto (Sr. Director of Product Management, Dec & Sec)
* Jennifer Garcia (EBA) supporting David Hong (VP of Field Operations), Michelle Hodges (VP of Worldwide Channels) and future VP of Alliances
* Victoria Reinhardt (EBA) supporting David Sakamoto (VP of Customer Success), Ryan O’Nell (VP of Commercial Sales), Mike Pyle (VP of Enterprise Sales)

## Contact us
We'd love for our fellow team members to contact us in slack at #eba-team. If you need to quickly communicate with the entire team please @ mention exec-admins in slack.


## Meeting request requirements

If you would like to schedule a meeting with E-Group at GitLab, please slack the EBA in #eba-team with the following information (or DM if confidential):
* Must have/optional attendees
* Meeting type: internal prep, client facing/customer, prospective customer, etc
* Urgency/ Desired timeframe: in the next two days, in the next week or two, etc
* Duration: 25 mins, 50 mins, etc.
* Subject of the meeting
* Desired outcome of the meeting
* Provide context: include agenda to be covered. Share google doc if available to be included in invite or link relevant issues, slides, etc.
* [Meeting Template](https://docs.google.com/document/d/1qj4MRlIXXGs4Jni0ITYp1uaHDQr53IvOqmpN-47RD-k/edit#heading=h.954v91mukl7r) should be used for all meetings with members of our E-Group and provided at the time of the meeting request. Select file > make a copy to use this template.
* [Customer Briefing Document](https://docs.google.com/document/d/1hyA12EN5iEwApAr_g4_-vhUQZohKxm5xkX9xxZ1JNog/edit?usp=sharing) should be used for all meetings with Customers or Prospects and provided to the EA at the time of the meeting request.  Select file > make a copy to use this template and please make sure it is editable by everyone at GitLab. Watch Nico Ochoa in [this 6.5-minute video](https://youtu.be/Wdg9YGQvs20) share his best practices for completing this template ([click here](https://docs.google.com/document/d/1rITvwVvv9Vn89qAgRX2Pf69zzoRp2AcqjtYBVUAria8/edit?usp=sharing) for Nico's template). If you have any questions related to this document, please ping in #eba-team in slack.
* If you have an urgent meeting request that is customer related, please provide the EBA with all of the above and a high level agenda if you do not have time to complete the briefing document in advance or if we are vetting prospect meetings with our E-Group.
* If you have a request for a member of E-Group to attend and speak at an event (keynote, panel, fireside chat, etc.), please fill out the [Executive External Event Brief](https://docs.google.com/document/d/1qMCw9gGTAdk3YW-AW2W_eCYXMGcNYyaq_wN0giSf-yk/edit) with the key information and email to the EBA supporting the Executive with your request. The EBA will review, provide feedback, and gain approval accordingly and confirm directly with you. If the request involves the CEO, please follow the process outlined here. The EBA to the CEO will gain approval from the function head of the requestor on whether the CEO will attend or not. Whether a new talk needs to be prepared or an existing one can be adjusted to the audience should be a part of the decision whether or not to participate in the event.
* [Partner Briefing Document](https://docs.google.com/document/d/1BR07oZcLwBOvkdz29mnr8i-rCGov2h3AkxaM036VcvY/edit#heading=h.6vzs0hxgy55p) should be used for all meetings with Partners  and provided to the EA at the time of the meeting request.  Select file > make a copy to use this template and please make sure it is editable by everyone at GitLab. If you have any questions related to this document, please ping in #eba-team in slack.

## EBA Team Best Practices

### Suggested formats for calendar invites within Executive Group (E-Group)
* MTG for meetings in person, either at Mission Control or another location
* INTERVIEW for media interviews (make sure to loop in our PR partner)
* CALL for phone calls or conference calls
* VIDEOCALL for video conference calls using Zoom
  * Example: “VIDEOCALL Jane Doe for Executive Assistant” or "VIDEOCALL Jane Doe (GitLab) & Sid Sijbrandij (GitLab)""
* LIVESTREAM for livestreams
  * When a meeting is being live-streamed to YouTube add Public Stream or Private Stream in the subject and body of the calendar invite to indicate which live stream
* A candidate interview will include the work interview using regular casing.
* 1:1 for one-on-ones with direct reports
  * Example: "1:1 Jane Doe & Sid"
* "Skip Level" should be in the title of skip level meetings
* Flight travel should include "flight" in the title
* Ground transportation is indicated by "Uber", "Ground Transportation", or "Car service" in the title
* Executive time should be labeled "Exec Time" or "Executive Time"
* Please utilize the [Zoom plugin for Google Calendar](/handbook/communication/#video-calls) to schedule Zoom meetings on behalf of E-Group
* Please add the subject of the call in the description, for internal and external calls.
* All external meetings RSVP should be confirmed with the guests at least 24hrs in advance
* When meetings are being rescheduled please add RESCHEDULING in the subject line of the calendar invite to indicate the reschedule and change the RSVP to "No"


When sending a calendar invite for an in-person meeting at Mission Control, please make sure to include the following items in the calendar invite:

1. Calendar blurb
1. Cell numbers (in case of an in-person meeting)
1. Mission Control Access Instructions (when meeting at Mission Control)

### General scheduling guidelines

* For meetings spanning across multiple time zones and with external parties, [Time & Date Calculator](https://www.timeanddate.com/worldclock/meeting.html) can help determine the best time to schedule
* When scheduling internally with multiple attendee's, please utilize Google [calendars](https://calendar.google.com)[Find a Time feature](https://gsuitetips.com/tips/calendar/find-a-time-in-google-calendar/) to see when GitLab team-members are free to schedule a meeting with. Please be cognizant of people time zones. If in doubt, please reach out to the team member directly in slack
* When researching flights, consider using a tool like [Skyscanner](https://www.skyscanner.com) or [Google Flights](https://www.google.com/flights?hl=en) to explore options for flights
* Schedule calls in European timezones in the morning (am) Pacific (Daylight) Time and US time zones in the afternoon (pm) Pacific (Daylight) Time
* Holds on the schedule should be removed at least 60 minutes before the preceding meeting starts. Preferably 24hr in advance of the meeting when possible
* Meetings should be scheduled for 25 minutes or 50 minutes.  As a general guideline meetings should not be scheduled to the full 30/60 minute mark
* If the call is with any Google company, use Hangouts instead of Zoom.
* For meetings at Mission Control and another guest joining via video call: The EBA will schedule an additional ten minutes before the appointment to test the zoom room system.
* For meetings or lunch/dinner appointments, always make sure to add the address in the invite of the location where it’s scheduled.
* Make sure to plan travel time (in a separate calendar item, just for the exec) before and after the meeting in case another meeting or call should follow.
* After each meeting with a potential investor, make sure to update Airtable with the information on these meetings.

## Scheduling with the Executive Team

Please remember to reach out to the EBA of the executive you need to schedule for with an agenda, any time preferences, and length of the meeting.

#### [Scheduling with the CEO](/handbook/eba/ceo-scheduling)
#### [Scheduling with the E-Group](/handbook/eba/e-group-scheduling)
#### [Scheduling with VPs or Directors](/handbook/eba/vp-scheduling)

### Travel

EBAs research the best option for a flight and book it according to their executive's schedule. If the Executive requires approving the flights beforehand, please follow that process before booking.
Make sure to add a calendar item for 2 hours before take off for check in and add a separate one for travel time before that in the exec's calendar.
If a flight was changed or not taken due to delays or other circumstances, make sure to check with the airline for the current flight status.

### Expensify
* When you’re logged in, you can find wingman account access for other team members in the top right corner menu.
* Check their email (if you have access), using the search bar in the top, to find any receipts for the postings in the current expense report.
* And/or write down what receipts are missing and email to request them if needed.

### Zoom
* Request Schedule Privilege for executives by having the executive go to their Zoom profile settings and scrolling down to the **Schedule Privilege** section. The executive will need to  click the plus button icon to add the EBA's email address to the **Assign Scheduling Privilege** pop-up box.
  * This setting will allow EBAs to create meetings using the user's One-time Meeting ID.
* To add yourself as an alternative host to a meeting, click on your profile and select **Meetings** from the header. Find the meeting you'd like to be an alternative host for, and click on the meeting title. Scroll to the bottom and select, **Edit this Meeting**. Scroll to the bottom and add yourself and/or others as **Alternative Hosts**.
  * This will allow you access to additional meeting settings such as Mute All, Unmute All, Play Enter/Exit Chime, Mute Participants on Entry, Record, End Meeting, and more.

### Google Groups
To view members of Google Groups, reference the [Google Workspace group member repo](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members/-/tree/master). A sync is run on the repository every hour, as noted in the README.md file.

### Company Mail
 The mail is forwarded weekly to the CFO's Sr. EBA and will be distributed accordingly.
* Mail addressed to Sytse 'Sid' Sijbrandij - contact the Staff Executive Business Admin to CEO for next steps.
* Incoming checks for deposit - scan and email to the PAO, Controller, Director of Revenue and Sr. Billing Manager; then send for deposit.
* Mail from Comerica - scan and email to the Controller and Sr. Billing Manager.
* Mail addressed to Legal - contact the CLO for next steps.
* Mail related to tax documents - scan and email to the PAO, Director of Tax and Controller.
* Mail related to employment, payroll, workers compensation, court orders, unemployment - mail to designated Payroll Specialiast.
* Mail addressed to AP - scan and email to ap@gitlab.com.
* All human resources related documents will be sent to a designated team member on the People Group Team.
* The Sr. EBA to the CFO is responsible for all remaining mail, which will be dispersed accordingly to specific individuals.
* More information can be found [here](https://docs.google.com/document/d/1EwaqZpivB2b7tozZNV6iaFzIpNePIM4HsLbk62Ym0lQ/edit#).

### E-Group In-person Meetings

There should be one invite for all attendees that includes the following:

* Exact meeting time blocked (ie: Start at 9am PST, End at 5pm PST)
* Zoom Link for remote participants
* Agenda (the agenda should also include the zoom link at the top)
* Notes doc shared via calendar invite and sharing set to "can edit" for those attending the meeting

## Public engagement

The public process does two things: allows others to benefit from the conversation and it acts as a filter since there is only a limited amount of time so we should prioritize conversations that a wider audience can benefit from.

### OKRs
* EBA to the CEO to assist in maintaining and scheduling meetings revolving around the [OKR updating process](/company/okrs/#updating).

## Performance Indicators (PI)

### Leadership SAT Survey

On a quarterly basis, any Executive at GitLab that has dedicated administrative support via an Executive Business Administrator will receive a survey with a series of questions and a rating scale of 1-5 (5 being excellent) to determine the performance of the EBA team.

Sample Questions:
* Tasks are completed within agreed timeframes, to accurately meet requirements and consistent with GitLab’s policies and procedures.
* EBA responds to queries in a timely manner based on the urgency of the request
* EBA knows and keeps Executive(s) informed of all activities and appointments at all times.
* EBA interacts and communicates clearly with key stakeholders both internally and externally through completion of tasks with little direction
* EBA proactively seeks feedback on the quality of administrative support and demonstrates an approach of continuous improvement

