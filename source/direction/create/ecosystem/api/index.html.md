---
layout: markdown_page
title: "Category Direction - API"
---

- TOC
{:toc}

| ------ | ------ |
| **Stage** | [Create](/direction/dev/#create-1) | 
| **Maturity** | [Viable](/handbook/product/categories/maturity/) |
| **Last reviewed** | 2021-01-16 |

- [Issue List](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AAPIs%2FSDKs)
- [Overall Ecosystem Direction](/direction/create/ecosystem/)

_This direction is a work in progress, and [everyone can contribute](#contributing):_

* Please comment, thumbs-up (or down!), and contribute to the linked issues and 
  epics on this category page. Sharing your feedback directly on GitLab.com is 
  the best way to contribute to our vision.
* Direct feedback can be shared via [email](mailto:pdeuley@gitlab.com), 
  [Twitter](https://twitter.com/gitlab) or on a video call.
* _If you're a GitLab user or developer and have direct feedback about our API, we'd love to hear from you._

## Overview

GitLab's **API** category provides all the tools and resources for 
developers to integrate externally with GitLab as a platform. Our [Integrations category](/direction/create/ecosystem/integrations/)
supports key integrations directly inside the GitLab codebase. 

Currently, a REST and GraphQL API are available for developers to use to 
integrate with GitLab. You can learn more about our APIs in [our developer documentation](https://docs.gitlab.com/ee/api/).

Beyond accessing these endpoints directly, [API Clients](/partners/#api-clients) 
and [CLI Clients](/partners/#cli-clients) that have been created by our community 
are availalbe for a variety of platforms and languages.

## Direction

As the GitLab community grows and increasingly relies on GitLab as a central 
part of their toolchain, it is critical that we provide support for integrating 
our product with a wider variety of systems. These integrations could be with 
project management systems, ERPs, custom dashboards and reporting systems, or 
any other type of custom-built tool. 

To better serve developers, each [product group](/handbook/product/categories/) 
across GitLab contributes new functionality to our APIs based on their product roadmaps.
The `API` category is responsible for guiding the path of this development on 
a holistic level.

Over the next year, we will work with key stakeholders to refine guidelines and 
best practices that will inform how these teams are designing and building their 
APIs and how they will ensure that these resources remain performant for 
consumers. Additionally, this team will act as a centralized resource, helping 
to triage critical problems with our APIs as the need arises.

## Future Vision

By providing powerful and flexible development resources, we allow creators to 
consume GitLab _as a platform_&mdash;giving them the freedom to create anything 
they may need. [Penflip](https://www.penflip.com/) is a great example of a novel 
product that was built upon GitLab that uses the core GitLab functionality to 
produce a whole new experience. These developers took GitLab's core functionality 
and reimagined it for a wholly different purpose and a totally different type of user. 

Making room for this sort of innovation and creativity will let us grow our 
community and impact in novel ways, and the more we expand the resources we 
provide, the more of this impact we will see. Over time, we would like to expand 
our developer tooling to allow more of this type of creativity and build a 
framework that allows us to better serve the broader variety of creative work, 
driving us closer to our [Big Hairy Audacious Goal](/company/mission-and-vision/#big-hairy-audacious-goal).

## Maturity

Today, we consider our **API** to be [Viable](/direction/maturity/). Below is 
how we think about how we'll grow that maturity level over time:

* **Viable** APIs give developers access to the majority of core GitLab 
  functionality and offer basic documentation covering the usage of the API. 
  _(Where we are today)_
* **Complete** APIs give developers access to almost all GitLab functionality 
  through direct API access, supported by robust documentation that covers 
  the usage of the API, best practices, and offers examples and sample code to 
  try yourself.
* **Lovable** APIs give developers access to almost all GitLab functionality 
  through direct API access, supported by robust documentation that covers 
  the usage of the API, best practices, and offers examples and sample code to 
  try yourself. Additionally, these resources are supplemented by well-supported 
  client libraries, CLI tools that make jumping in and developing simple.

_[Learn more about how GitLab thinks about Maturity here.](/direction/maturity/)_

## What's next and why

Based on these goals, our current priorities are:

* **[Create a GitLab Developer Portal](https://gitlab.com/groups/gitlab-org/-/epics/2177)** 
  to make it easier for developers to find the resources that are currently available.
* **Refine our API guidelines and best practices** 
  to ensure a more consisently great experience across our APIs, and make it 
  clearer to our contributors what they need to be building, and why.
* **[Establish product analytics for our API and SDK](https://gitlab.com/gitlab-org/gitlab/issues/34095)** 
  to allow us to better understand how these resources are being used, and give 
  us insight to which areas need improvement or expansion.

## What we're not doing

### Building "all" of GitLab's APIs

Each [Group](/handbook/product/categories/) is ultimately responsible for 
building and maintaining their own APIs. Ecosystem's role is to be a source of 
guidance and governance across them as a whole. 

### Creating new API client libraries

There are currently [many client libraries](/partners/#api-clients) that were 
created and are maintained by our community. We don't currently have any 
plans on creating our own libraries, but are happy to support the efforts 
of those contributors and community members.

## Contributing

At GitLab, one of our values is that everyone can contribute. If you're looking 
to contribute to our API or otherwise get involved with features in the 
Ecosystem area, [you can find open issues here](https://gitlab.com/gitlab-org/gitlab/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aecosystem&label_name[]=Category%3AAPIs%2FSDKs).

Feel free to reach out to the team directly if you need guidance or want 
feedback on your work by pinging [@deuley](https://gitlab.com/deuley) or 
[@gitlab-ecosystem-team](https://gitlab.com/gitlab-org/ecosystem-team) on your 
open MR.

You can read more about our general contribution guidelines [here](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

## Feedback & Requests

If there's an integration that you'd like to see GitLab offer, or if you have 
feedback about an existing integration: please [submit an issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new?issue%5Btitle%5D=New%20GitLab%20Integration%20with) with the label 
`~Category:Integrations`, or contact [Patrick Deuley](mailto:pdeuley@gitlab.com), 
Sr. Product Manager, Ecosystem.
