title: Glympse
cover_image: '/images/blogimages/glympse_case_study.jpg'
cover_title: |
  Glympse is making geo-location sharing easy
cover_description: |
  With GitLab, Glympse is able to improve security scanning and deploy time
canonical_path: "/customers/glympse/"
twitter_image: '/images/blogimages/glympse_case_study.jpg'

customer_logo: '/images/case_study_logos/glympse-logo.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Seattle, Washington
customer_solution: Ultimate
customer_employees: 30 employees
customer_overview: |
   Glympse is a fast, free and simple way to share your real-time location and estimated arrival time using GPS tracking. The tracking is temporary and secure – and Glympse recipients don’t need to download an app to see the shared location.
customer_challenge: |
   A complex developer tech stack with over 20 distinct tools that was hard to maintain and manage. Teams spent several hours a week keeping tools running rather than shipping innovation to their app.

key_benefits:
  - |
    Teams over 2x more efficient. Teams using GitLab completed over 50 days of work (21 workdays)
  - |
    Developers can deploy to production without fear of disparate systems and worry over versioning
  - |
    Significantly streamlined code-review-test-deploy process through pipelines
  - |
    Reduced deployment fatigue enabling code to be shipped more frequently

customer_stats:
  - stat: <30 mins
    label: deploys from 4 hours
  - stat: 5 teams
    label: using GitLab 100%
  - stat: ~20 tools
    label: consolidated into GitLab

customer_study_content:
  - title: the customer
    subtitle:  Tracking the last mile of delivery
    content:
      - |
        Watching food delivery or seeing when a repair person will arrive is powered by
        real-time location tracking. But once the food is delivered or the service is complete, you want your
        location forgotten. Glympse Inc. technology provides end-users with a temporary real-time location tracking
        platform to share their location. Glympse works with retailers and home service providers around the
        globe to provide real-time location sharing in their last-mile offerings.
      - |




  - title: the challenge
    subtitle: Overcoming a disparate toolchain
    content:
      - |
        At the end of 2017, Glympse was faced with the challenges associated with disparate processes. Code management
        and reviews were performed in different tools than pipelines were run in. At that time, pipelines consisted of
        disjointed Jenkins jobs. They tried Shippable, which improved the process a bit, but they still weren’t able to
        connect merge requests to production. 
      - |


  - blockquote: We had something like 20 odd different tools to kind of wrap around the system that we already had. But luckily,
        our leadership understood the importance of simplifying our processes and once we got GitLab in, we were off to the
        races
    attribution: Cillian Dwyer
    attribution_title: Site Reliability Engineer, Glympse

  - title: the solution
    subtitle: GitLab Gold is making the audit process easier
    content:
      - |
        Glympse is in the process of earning a SOC 2 Type II audit and GitLab is vital to achieving the certification. Because
        Glympse is using Gold they can leverage built-in language agnostic CI pipelines. This allowed them to quickly respond to
        auditor’s feedback on the compliances of over 50 repositories and build a complete security package for integrating code
        changes into their environment.
      - |
        One of the senior auditors commented in passing that having the code quality, the SAST and container scanning and the
        pipeline, all automated in GitLab is almost better than a manual review. “My response was, ‘Well, we're going to keep the
        manual review, that's part of our process’ but it's cool that he was almost okay with, not needing another developer for review.
        The security jobs in place are catching vulnerabilities from migrating to production through the product" explained Zaq? Wiedmann,
        lead software engineer.
      - |
        Wiedmann said the auditor also mentioned that Glympse had remediated security issues faster than any other company that he had
        worked with before in his 20-year career. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all
        of their repositories using GitLab's CI templates and their pre-existing Docker-based deployment scripts.



  - title: the results
    subtitle: Improving deployment speed by 8x
    content:
      - |
       The team fully integrated GitLab into their environment in January 2019 over the course of a single month. GitLab allowed the teams to
       suggest a merge request, run unit tests on it, then automatically build a new Docker image which is deployed to the sandbox environment.
       GitLab triggers tests in the sandbox and production deploys which are all managed on auto-scaling GitLab runners.
      - |
       “The managers are excited (about GitLab) because it helps reduce the amount of time we spend on things that we don't need to be spending
       time on. Focusing on the important stuff basically, get back to actually engineering and not focusing on building weird pipelines with Jenkins
       and Shippable and GitHub and trying to hook everything together in crazy scripts and stuff”— Cillian Dwyer, site reliability engineer.
      - |
       Glympse wired their GitLab pipelines to AWS and deploy directly into their VPCs across the world. Thanks to deploy environments Glympse is able to
       track and version across production and staging environments.
      - |
       Glympse is also using all of the GitLab security jobs including SAST and DAST for static and dynamic applications security testing. Additionally,
       the company has container scanning, code quality, and license compliance jobs running. Jobs are managed within templates and imported by all production services.
      - |
       “GitLab has had a positive effect on our culture. Everyone feels better about shipping code and deployments. There is more confidence in the org and deployment
       is a non-issue,” — Zaq? Wiedmann, lead software engineer.
      - |
  - blockquote: Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined
    attribution: Zaq? Wiedmann
    attribution_title: Lead Software Engineer


