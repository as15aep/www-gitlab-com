description: Pipeline as code uses source code, like Git, to create continuous
  integration pipelines. Learn the benefits of pipeline as code and how teams
  use it to configure builds and automate tests.
canonical_path: /topics/ci-cd/pipeline-as-code/
parent_topic: ci-cd
file_name: pipeline-as-code
twitter_image: /images/opengraph/gitlab-blog-cover.png
title: What is pipeline as code?
header_body: Pipeline as code is a practice of defining deployment pipelines
  through source code, such as Git. Pipeline as code is part of a larger “as
  code” movement that includes infrastructure as code.
body: >-
  Pipeline as code is a practice of defining deployment pipelines through source
  code, such as Git. Pipeline as code is part of a larger “as code” movement
  that includes infrastructure as code. Teams can configure builds, tests, and
  deployment in code that is trackable and stored in a centralized source
  repository. Teams can use a
  declarative [YAML](https://about.gitlab.com/blog/2020/10/01/three-yaml-tips-better-pipelines/) approach
  or a vendor-specific programming language, such as Jenkins and Groovy, but the
  premise remains the same.


  A pipeline as code file specifies the stages, jobs, and actions for a pipeline to perform. Because the file is versioned, changes in pipeline code can be tested in branches with the corresponding application release.


  The pipeline as code model of creating [continuous integration pipelines](https://about.gitlab.com/blog/2019/07/12/guide-to-ci-cd-pipelines/) is an industry best practice, but deployment pipelines used to be created very differently.


  Early in continuous integration, deployment pipelines were set up as point-and-click or through a graphical user interface (GUI). This presented several challenges:


  * Auditing was limited to what was already built-in

  * Developers were unable to collaborate

  * Troubleshooting problems was difficult

  * Difficult to rollback changes to the last known configuration

  * Pipelines prone to breaking


  ## What are the benefits of pipeline as code?[](https://about.gitlab.com/topics/ci-cd/pipeline-as-code/#what-are-the-benefits-of-pipeline-as-code)


  The pipeline as code model corrected many of these pain points and offered the flexibility teams needed to execute efficiently.


  Pipeline as code comes with many of the [same benefits](https://martinfowler.com/bliki/InfrastructureAsCode.html) the other "as code" models have, such as:


  * **[Version control](https://about.gitlab.com/topics/version-control/)** – Changes are trackable and teams can rollback to previous configurations.

  * **Audit trails** – Developers can see when changes were made to the source code and why.

  * **Ease of collaboration** – Code is available and visible for improvements, suggestions, and updates.

  * **Knowledge sharing** – Developers can share best practices, import templates, and link code snippets so teams can learn from each other.


  Pipeline as code also has operational and practical benefits:


  1. CI pipelines and application code are stored in the same source repository. All the information teams need is located in the same place.

  2. Developers can make changes without additional permissions and can work in the tools they’re already using.

  3. Teams can collaborate more efficiently. Keeping information accessible means teams can collaborate and then act on their decisions.

  4. Pipeline changes go through a code review process, avoiding any break in the pipeline integration.

  5. Deployment pipelines are in a version control system independent of continuous integration tools. Pipelines can be restored if the continuous integration system goes down. If a team wants to switch CI tools later on, pipelines can be moved into a new system.


  The pipeline as code model creates automated processes that help developers build applications more efficiently. Having everything documented in a [source repository](https://docs.gitlab.com/ee/user/project/repository/) allows for greater visibility and collaboration so that everyone can continually improve processes.


  ## Get started with CI/CD[](https://about.gitlab.com/topics/ci-cd/pipeline-as-code/#get-started-with-cicd)


  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/sIegJaLy2ug" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
cta_banner:
  - title: What is Auto DevOps?
    body: GitLab Auto DevOps eliminates the complexities of software delivery by
      automatically setting up the pipeline and integrations, which allows teams
      to focus on business productivity.
    cta:
      - text: Learn More
        url: https://about.gitlab.com/stages-devops-lifecycle/auto-devops/
resources_title: More on CI pipelines
resources:
  - url: https://about.gitlab.com/customers/paessler-prtg/
    type: Case studies
    title: How Paessler deploys up to 50 times daily with GitLab Premium
  - title: Mastering continuous software development
    url: /webcast/mastering-ci-cd/
    type: Webcast
  - title: Scaled continuous integration and delivery
    url: /resources/scaled-ci-cd/
    type: Whitepapers
  - title: The benefits of single application CI/CD
    url: /resources/ebook-single-app-cicd/
    type: Whitepapers  
suggested_content:
  - url: /blog/2018/10/31/use-multiproject-pipelines-with-gitlab-cicd/
  - url: /blog/2019/06/27/positive-outcomes-ci-cd/
  - url: /blog/2019/06/21/business-impact-ci-cd/
